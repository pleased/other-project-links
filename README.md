These are links to various projects and data sets from past and current students in the PLeaSeD research group related to their work while in the group, which are not hosted on the PLeaSeD group page.

# Software tools

* [Oakfoam](https://bitbucket.org/francoisvn/oakfoam/wiki/Home) - a Computer Go agent
* [The Ingenious Framework](https://bitbucket.org/skroon/ingenious-framework) - a game-playing framework for artificial agents

# Models

* [Milling circuit](https://github.com/ProcessMonitoringStellenboschUniversity/ME-milling-circuit) simulation model and simulation data (uses MATLAB and Simulink).  Based on the paper "Analysis and validation of a run-of-mine ore grinding mill circuit model for process control" (published in Minerals Engineering), the model was developed and presented at MEi Comminution (2016).  The model was initially used for analyses in "Monitoring of a simulated milling circuit: Fault diagnosis and economic impact" (published in Minerals Engineering).

# Data

* [Pothole data set](http://staff.ee.sun.ac.za/mjbooysen/Potholes) for the paper: "Detecting potholes using simple image processing techniques and real-world footage" (SA Transport Conference 2015)

# Reproducible research

* [Code for reproducing results](https://github.com/arnupretorius/lindaedynamics_icml2018) in the paper: "Learning dynamics of linear denoising autoencoders." (ICML 2018)
* [Demonstration code](https://github.com/ProcessMonitoringStellenboschUniversity/IFAC-VAE-Imputation) for experiments from the paper: "Variational Autoencoders for Missing Data Imputation with Application to a Simulated Milling Circuit" (IFAC-MMM2018)
* [Code for reproducing results](https://github.com/ElanVB/noisy_signal_prop) in the paper: "Critical initialisation for deep signal propagation in noisy rectifier neural networks." (NIPS 2018)
* [Repository](https://github.com/felixmcgregor/Bayesian-Neural-Networks-with-self-stabilising-priors) accompanying the paper "Bayesian neural networks with self stabilising priors" (NeurIPS 2019 workshop on Bayesian Deep Learning)
